import { withContext, Context } from '@extism/extism';
import { readFileSync } from 'fs';


let ctx = new Context();
let wasm = readFileSync('../hello-world/hello-world.wasm');
let p = ctx.plugin(wasm, true);

if (!p.functionExists('helloWorld')) {
  console.log("no function 'helloWorld' in wasm");
  process.exit(1);
}

let buf = await p.call('helloWorld', process.argv[2] || '😄 Hey!');
console.log(JSON.parse(buf.toString()));
p.free();
