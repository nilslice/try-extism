# Extism
> https://extism.org/


## Install Extism

### Install Extism CLI

> https://extism.org/docs/install/

```bash
curl https://raw.githubusercontent.com/extism/cli/main/install.sh | sh
pip3 install extism
```

### Install the latest Extism library and dependencies

```bash
extism install latest
```

## Create an Extism Plugin (with Go and TinyGo)

```bash
mkdir hello-world
cd hello-world
go mod init hello-world
go get github.com/extism/go-pdk
```

export PATH="/home/gitpod/.local/bin:$PATH"


