package main

import (
	"github.com/extism/go-pdk"
)

//export helloWorld
func helloWorld() int32 {
	input := pdk.Input()
    
  	pdk.Log(pdk.LogDebug, string(input))

	output := `{"message": "👋 Hello World 🌍"}`

	mem := pdk.AllocateString(output)
	// zero-copy output to host
	pdk.OutputMemory(mem)

	// alternatively, you can condense the two operations above to:
	// pdk.Output([]byte(output)) // however, this adds an extra copy!

	return 0
}

func main() {}
